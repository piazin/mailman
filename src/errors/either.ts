export type Either<F, S> = Failed<F, S> | Sucess<F, S>;

class Failed<F, S> {
  readonly value: F;

  constructor(value: F) {
    this.value = value;
  }

  isFailed(): this is Failed<F, S> {
    return true;
  }

  isSucess(): this is Sucess<F, S> {
    return false;
  }
}

export class Sucess<F, S> {
  readonly value: S;

  constructor(value: S) {
    this.value = value;
  }

  isFailed(): this is Failed<F, S> {
    return false;
  }

  isSucess(): this is Sucess<F, S> {
    return true;
  }
}

export const failed = <F, S>(l: F): Either<F, S> => {
  return new Failed(l);
};

export const sucess = <F, S>(r: S): Either<F, S> => {
  return new Sucess(r);
};
