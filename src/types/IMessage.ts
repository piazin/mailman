export interface IMessage {
  to: string;
  from: string;
  subject: string;
  html: string;
  reply_to?: string;
  cc?: string | (string | undefined)[] | undefined;
}
