export interface RequestBody {
  cc?: string;
  name: string;
  email: string;
  message: string;
  subject?: string;
  _redirect: string;
}
