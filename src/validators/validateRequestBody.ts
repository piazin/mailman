import { z } from 'zod';
import { RequestBody } from '../types';

const emailSchema = z.string().trim().email('Email invalido').optional();
const requestSchema = z.object({
  name: z.string({ required_error: 'O campo name é obrigatório' }),
  email: z
    .string({ required_error: 'O campo email é obrigatório' })
    .email('Email invalido!')
    .min(5),
  subject: z
    .string()
    .min(5, 'o campo subject deve ter no minimo 5 caracteres')
    .max(100, 'o campo subject deve ter no maximo 100 caracteres')
    .optional(),
  message: z.string({ required_error: 'O campo message é obrigatório' }).min(1).max(2000),
  _redirect: z.string().trim().url('Url de redirecionamento invalida!').optional(),
  cc: z.array(emailSchema).optional(),
});

function parseAndValidateRequestBody(body: RequestBody) {
  const { cc, ...rest } = body;
  const copyEmails = cc?.split(',');

  return requestSchema.parse({ cc: copyEmails, ...rest });
}

export function validateRequestBody(body: RequestBody) {
  try {
    const parsedBody = parseAndValidateRequestBody(body);
    return {
      bodyError: null,
      body: parsedBody,
    };
  } catch (err) {
    if (err instanceof z.ZodError) {
      console.error(err);
      return {
        bodyError: err.errors[0]?.message ?? 'Erro na validação',
        body: null,
      };
    }
    return {
      bodyError: 'Ops! Requisição inválida',
      body,
    };
  }
}
