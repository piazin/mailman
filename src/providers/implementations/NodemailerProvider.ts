import nodemailer, { Transporter } from 'nodemailer';
import { IMailProvider, Response } from '../IMailProvider';
import { transportConfig } from '../../config/nodemailer.config';
import { failed, sucess } from '../../errors/either';
import { ErrorToSendEmail } from '../../errors/baseError';

interface IMessage {
  to: string;
  from: string;
  subject: string;
  html: string;
}

export class NodemailerProvider implements IMailProvider<Transporter, IMessage> {
  transporter: nodemailer.Transporter;

  constructor() {
    this.transporter = nodemailer.createTransport(transportConfig);
  }

  async sendMail(message: IMessage): Promise<Response> {
    try {
      await this.transporter.sendMail(message);
      return sucess('Sucesso ao enviar email!');
    } catch (error) {
      return failed(new ErrorToSendEmail('falha ao enviar o email!', 500));
    }
  }
}
