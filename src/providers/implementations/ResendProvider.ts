import { Resend } from 'resend';
import { IMailProvider, Response } from '../IMailProvider';
import { resendConfig } from '../../config/resend.config';

import { failed, sucess } from '../../errors/either';
import { ErrorToSendEmail } from '../../errors/baseError';

interface IMessage {
  to: string;
  from: string;
  subject: string;
  html: string;
  attachments?: [
    {
      path: string;
      content: string;
      filename: string;
    }
  ];
}

export class ResendProvider implements IMailProvider<Resend, IMessage> {
  transporter: Resend;

  constructor() {
    this.transporter = new Resend(resendConfig.key);
  }

  async sendMail(message: IMessage): Promise<Response> {
    try {
      await this.transporter.emails.send(message);
      return sucess('Sucesso ao enviar o email!');
    } catch (error) {
      console.log('🚀 ~ file: ResendProvider.ts:20 ~ ResendProvider ~ sendMail ~ error:', error);
      return failed(new ErrorToSendEmail('Falha ao enviar o email!', 500));
    }
  }
}
