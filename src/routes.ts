import multer from 'multer';
import { Router } from 'express';
import multerConfig from './config/multer.config';
import { sendEmailController } from './useCases/SendEmail';

const router = Router();

const upload = multer(multerConfig);

router.get('/', (req, res) => res.json('Hello from Mailman!'));

router
  .route('/send/:sender')
  .post(upload.single('file'), (req, res) => sendEmailController.handle(req, res));

export { router };
