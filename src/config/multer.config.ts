import path from 'path';
import multer from 'multer';
import crypto from 'crypto';
const tmpFolder = path.resolve('tmp');

export default {
  dest: tmpFolder,
  storage: multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, tmpFolder);
    },
    filename: function (req, file, cb) {
      if (!file) return cb(null, '');

      const uuid = crypto.randomUUID();
      const filename = `${uuid}-${file.originalname}`;

      cb(null, filename);
    },
  }),
  limits: {
    fileSize: 2 * 1024 * 1024,
  },
  fileFilter: (req, file, cb) => {
    if (!file) return cb(null, true);
    const allowedMimes = ['image/jpeg', 'image/pjpeg', 'image/png', 'application/pdf'];

    if (allowedMimes.includes(file.mimetype)) {
      cb(null, true);
    } else {
      cb(new Error('Invalid file type.'));
    }
  },
} as multer.Options;
