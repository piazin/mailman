import { app } from './app';
import { AppClusterService } from './infra/AppClusterService';

const bootstrap = () => {
  app.listen(process.env.PORT || 3000, () => console.log('🚀 ~ server started'));
};

bootstrap();
// AppClusterService.clusterize(bootstrap);
