import path from 'path';
import fs from 'fs/promises';
import { failed } from '../../errors/either';
import { ISendEmailDTO } from './SendEmailDTO';
import { ErrorToSendEmail } from '../../errors/baseError';
import { MailProvider, Response } from '../../providers/IMailProvider';
import { replaceTemplateVariables } from '../../helpers/replaceTemplateVariables';
import { availableTemplates, getEmailTemplate } from '../../templates/getEmailTemplate';

export class SendEmailUseCase {
  constructor(private mailProvider: MailProvider) {}

  async execute(data: ISendEmailDTO): Promise<Response> {
    try {
      const { to, cc, name, email, message, subject, from, attachment } = data;
      const authorizedEmails = ['ls4803326@gmail.com'];

      if (!authorizedEmails.includes(to)) {
        return failed(new ErrorToSendEmail('Email não autorizado', 403));
      }

      const emailTemplate = await getEmailTemplate(availableTemplates.Default);
      const emailContent = replaceTemplateVariables(emailTemplate as string, {
        name: name as string,
        email: email as string,
        message: message as string,
      });

      const attachments = [];
      if (attachment) {
        const attachmentBuffer = await fs.readFile(path.resolve(attachment!.path), 'base64');
        const { path: attPath, filename } = attachment;

        attachments.push({ path: attPath, filename, content: attachmentBuffer });
      }

      const mailOptions = {
        to,
        cc,
        from,
        html: emailContent,
        subject: subject || 'Novo email recebido do seu formulario Mailman',
        attachments,
      };

      return await this.mailProvider.sendMail(mailOptions);
    } catch (error) {
      return failed(new ErrorToSendEmail('Falha ao enviar email!'));
    } finally {
      const { attachment } = data;

      if (attachment) {
        await fs.unlink(path.resolve(attachment?.path));
      }
    }
  }
}
