export interface ISendEmailDTO {
  to: string;
  from: string;
  name?: string;
  email?: string;
  subject?: string;
  message?: string;
  reply_to?: string;
  attachment?: Express.Multer.File;
  cc?: string | (string | undefined)[] | undefined;
}
