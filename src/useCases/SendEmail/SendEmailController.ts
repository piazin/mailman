import { Request, Response } from 'express';
import { SendEmailUseCase } from './SendEmailUseCase';
import { senderValidator } from '../../validators/senderValidator';
import { validateRequestBody } from '../../validators/validateRequestBody';

export class SendEmailController {
  constructor(private sendEmailUseCase: SendEmailUseCase) {}

  async handle(req: Request, res: Response): Promise<Response | void> {
    try {
      const { bodyError, body } = validateRequestBody(req.body);
      if (bodyError || !body) return res.status(400).json({ statusCode: 400, message: bodyError });

      const { senderError, value: sender } = senderValidator(req.params as unknown as string);
      if (senderError || !sender)
        return res.status(400).json({ statusCode: 400, message: senderError });

      const { email, message, name, subject, cc, _redirect } = body;

      const sendEmailResult = await this.sendEmailUseCase.execute({
        cc,
        name,
        email,
        message,
        subject,
        to: sender,
        from: 'mailman@lucasouza.tech',
        attachment: req.file,
      });

      if (sendEmailResult.isFailed()) {
        const { statusCode, message } = sendEmailResult.value;

        return res.status(statusCode).json({
          status: statusCode,
          message: message,
        });
      }

      const successMessage = 'Email enviado com sucesso';
      return _redirect
        ? res.redirect(_redirect)
        : res.status(200).json({ statusCode: 200, message: successMessage });
    } catch (error) {
      res.status(500).json({ status: 500, message: 'Internal Server Error' });
    }
  }
}
